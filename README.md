# Differential robot cyclic simulation
This repository is a ROS2 package for sending cyclic commands to a differential robot simulated on Gazebo


## Getting started
This repository uses an existing differential robot gazebo model.
Below are the prerequisite: 
1. ROS2 foxy
2. [ros2_control_demos](https://github.com/ros-controls/ros2_control_demos) -foxy branch

## Build
1. Go to your ros2 workspace and browse to the src folder
2. Clone the ros2_control_demos: foxy branch 
3. Clone the diffbot_cyclic_sim repository
4. colcon build your ros2 workspace

## Run
1. Run the command to launch differential robot on gazebo: ros2 launch ros2_control_demo_bringup diffbot_system.launch.py
2. On the second terminal run the cyclic command node: ros2 run diffbot_cyclic_sim diffbot_node
3. To parse the commands from yaml file, run command on the third terminal: ros2 param load /diffbot_node "workspace_path"/src/diffbot_cyclic_sim/config/diffbot_node.yaml

## Author
Shruti Patankar
