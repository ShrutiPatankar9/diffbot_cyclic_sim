#!/usr/bin/python3.8
from queue import Empty
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist

#Global variable declaration
pub = Empty

class CyclicCommand(Node):
    global pub

    #constructor
    def __init__(self):
        super().__init__('diffbot_node')
        self.timer = self.create_timer(0.1, self.timed_callback)

        self.declare_parameter('linear.x', 0.0)
        self.declare_parameter('angular.z', 0.0)

    # For fetching command parameters and publishing
    def timed_callback(self):
         self.linear_param = self.get_parameter('linear.x').get_parameter_value()
         self.angular_param = self.get_parameter('angular.z').get_parameter_value()

         msg = Twist()
         msg.linear.x = self.linear_param.double_value
         msg.angular.z = self.angular_param.double_value
         pub.publish(msg)
    
    

def main():
    global pub
    rclpy.init()
    node = CyclicCommand()

    pub=node.create_publisher(Twist,'/diffbot_base_controller/cmd_vel_unstamped',10)
    
    rclpy.spin(node)
    rclpy.shutdown()

 
    
if __name__ == '__main__':
    main()